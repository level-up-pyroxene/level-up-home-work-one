package StackAndQueue;

public class LinkedList {

    private Node first = null;

    public void addFirst(int nodeData){
        Node node = new Node(nodeData);
        node.setNext(first);
        setFirst(node);
    }

    public int removeFirst(){
        Node oldFirst = first;
        first = first.getNext();
        return oldFirst.getNodeData();
    }

    public void soutList(){
        Node current = first;
        while(current != null){
            System.out.print(" "+current.toString()+" ");
            current = current.getNext();
        }
        System.out.println();
    }

    public boolean isEmpty(){
        return (first == null);
    }

    private void setFirst(Node first){
        this.first = first;
    }

}
