package StackAndQueue;

public class Stack {

    private LinkedList linkedList = new LinkedList();

    public void push(int data) {
        //adds first
        linkedList.addFirst(data);
    }

    public int pop() {
        //returns an element
        //removes it from
        return linkedList.removeFirst();
    }
    //peek method?

    public void soutStack() {
        linkedList.soutList();
    }

    public boolean isEmpty(){
        return linkedList.isEmpty();
    }
}
