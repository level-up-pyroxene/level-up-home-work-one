package StackAndQueue;

public class QueueLinkedList {
    private class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
            this.next = null;
        }
    }

    private Node first, last;

    public QueueLinkedList() {

        this.first = this.last = null;

    }

    public void enqueue(int value) {
        Node node = new Node(value);
        if (this.last == null) {
            this.first = this.last = node;
            return;
        }
        this.last.next = node;
        this.last = node;
    }

    public Node dequeue() {
        //if isEmpty return Null
        if (isEmpty()) {
            return null;
        }

        //Store previous front and move front
        //one node ahead
        Node temporaryNode = this.first;
        this.first = this.first.next;
        //if front becomes null
        //change rear also as null
        if (this.first == null) {
            this.last = null;
        }
        return temporaryNode;
    }

    public boolean isEmpty() {
        return (first == null);
    }

    @Override
    public String toString() {
        StringBuilder strBuidlder = new StringBuilder();
        Node temporaryNode = first;
        while (temporaryNode != null) {
            strBuidlder.append(temporaryNode.value).append(", ");
            temporaryNode = temporaryNode.next;
        }
        return "Queue: " + strBuidlder.toString();
    }
}
