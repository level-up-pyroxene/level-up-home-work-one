package StackAndQueue;

public class Node {

    private int nodeData;
    private Node next;


    public Node(int nodeData){
        this.nodeData = nodeData;
    }

    public String toString(){
        return Integer.toString(nodeData);
    }

    //Getters\Setters
    public int getNodeData() {
        return nodeData;
    }

    public void setNodeData(int nodeData) {
        this.nodeData = nodeData;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
