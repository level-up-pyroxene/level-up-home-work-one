package ShopHomeWork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Storage implements IStorage {

    private static ArrayList<Item> itemStorage = new ArrayList<>();



    private Storage() {
    }

    private static Storage instance = new Storage();

    static Storage getInstance() {
        return instance;
    }

    public void sortByCategory(){
        itemStorage.sort((Comparator<? super Item>) itemStorage);
    }

    @Override
    public void addFromScan(InputReader inputReader) {
        System.out.println("Type in parameters with Enter after each");
        System.out.println("1 - Parameter, 2 - Name, 3 - Price");
        String category = inputReader.getInput();
        String name = inputReader.getInput();
        String price = inputReader.getInput();
        Item item = new Item(category, name, price);
        addItem(item);
        System.out.println();
        System.out.println(item.toString() + " is now added to the Storage");

    }

    @Override
    public void addItem(Item item) {
        itemStorage.add(item);
    }

    @Override
    public List<Item> getItemStorage() {
        return itemStorage;
    }

    public Item getItemFromIndex(int index){
        return itemStorage.get(index);
    }

    @Override
    public void getListedStoredItems() {
        int size = 0;

//        IntStream.range(0, itemStorage.size())
//                .forEach(i -> System.out.println(i + " " + itemStorage.get(i)));
//
//        getItemStorage().forEach(item -> System.out.println(size + " " + item));
//
        for (Item object : getItemStorage()) {
            System.out.println(size + " " + object);
            size++;
        }
    }
}
