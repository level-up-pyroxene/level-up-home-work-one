package ShopHomeWork;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Item {

    private String[] propertiesArray = new String[3];

    private String category = propertiesArray[0];
    private String name = propertiesArray[1];
    private String price = propertiesArray[2];

    public Item(String category,String name,String price){
        propertiesArray[0] = category;
        propertiesArray[1] = name;
        propertiesArray[2] = price;
    }

    @Override
    public String toString() {
        return Arrays.toString(propertiesArray);
    }

    public String[] getPropertiesArray() {
        return propertiesArray;
    }
}
