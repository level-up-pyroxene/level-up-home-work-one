package ShopHomeWork;

import java.util.ArrayList;
import java.util.List;

public interface IStorage {

    void addFromScan(InputReader inputReader);

    void addItem(Item item);

    List<Item> getItemStorage();

    void getListedStoredItems();

    Item getItemFromIndex(int index);

}
