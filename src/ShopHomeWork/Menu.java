package ShopHomeWork;

public class Menu {

    public void startingMenu(){
        System.out.println("|____________________________________________|");
        System.out.println("|                                            |");
        System.out.println("|              ---MAIN MENU---               |");
        System.out.println("|-__________________________________________-|");
        System.out.println();
        System.out.println("            ---Choose wisely---               ");
        System.out.println("   Type 1 to buy a something.");
        System.out.println("   Type 2 to add an item in the storage.");
        System.out.println("   Type 3 to exit");
        System.out.println("----------------------------------------------");
    }

    public void buyMenu(){
        System.out.println("|          ---SHOPPING MENU---           |");
    }
    public void boughtMenu(String itemBought){
        System.out.println("Congrats! You've just purchased "+itemBought);
    }

    public void addMenu(){
        System.out.println("|           ---ADDING MENU---            |");
    }

    public void continueMenu(){
        System.out.println("|         ---Type 1 to Continue---       |");
    }

}
