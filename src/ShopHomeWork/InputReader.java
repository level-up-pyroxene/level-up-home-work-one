package ShopHomeWork;

import java.util.Scanner;

public class InputReader {

    private Scanner scanner = new Scanner(System.in);

    private InputReader(){}
    private static InputReader instance = new InputReader();
    public static InputReader getInstance(){
        return instance;
    }

    public String getInput(){
        return scanner.next();
    }

    public int getPick(){
        return scanner.nextInt();
    }

}
