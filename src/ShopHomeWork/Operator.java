package ShopHomeWork;

public class Operator {
    Menu menu = new Menu();
    ShopUtil util = new ShopUtil();
    InputReader reader = InputReader.getInstance();

    public void run() {


        util.insertPreBuilt();
        while (true) {
            menu.startingMenu();
            //            IStorage storage = Storage.getInstance();
            int userPick = reader.getPick();
            if (userPick == 1) {
                menu.buyMenu();
                String itemStr = runBuyMethod();
                menu.boughtMenu(itemStr);
                runAskContinue();
            }else if (userPick == 2) {
                menu.addMenu();
//                storage.addFromScan(reader);
                util.addItem();
                runAskContinue();
            }else if (userPick == 3) {
                break;
            }
        }

    }

    private String runBuyMethod() {
        ShopUtil util = new ShopUtil();
        return util.buyItem();
    }
    private void runAskContinue(){
        menu.continueMenu();
        reader.getPick();
    }


}
