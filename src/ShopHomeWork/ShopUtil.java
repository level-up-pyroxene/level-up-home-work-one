package ShopHomeWork;

import com.sun.xml.internal.ws.util.StringUtils;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.function.Consumer;

public class ShopUtil {

    IStorage storage = Storage.getInstance();

    public void insertPreBuilt() {
        Item item = new Item("Laptop", "Lenovo", "1500");
        Item item2 = new Item("Vape", "Wismec", "150");
        Item item3 = new Item("Keyboard", "Razer", "400");
        Item item4 = new Item("Laptop", "Dell", "1300");
        Item item5 = new Item("Keyboard", "Logitec", "100");
        Storage.getInstance().addItem(item);
        Storage.getInstance().addItem(item2);
        Storage.getInstance().addItem(item3);
        Storage.getInstance().addItem(item4);
        Storage.getInstance().addItem(item5);
    }

    public String buyItem() {
        getStoragedListed();
        InputReader reader = InputReader.getInstance();
        int pick = reader.getPick();
        //call overrloaded method (int or string)
        //pass input
        String str = storage.getItemFromIndex(pick).toString();
        storage.getItemStorage().remove(pick);
        return str;
    }


    public void addItem() {

        InputReader reader = InputReader.getInstance();
        storage.addFromScan(reader);
    }


    private void getStoragedListed(){
        System.out.println("Pick any position and type corresponding index");
        storage.getListedStoredItems();
    }
}
