package HomeWorkThree.MinMax;

public class ArrayUtil {

    int getMin(int[] array) {
        int min = array[0];
        for (int num : array) {
            if (num < min) {
                min = num;
            }

        }
        System.out.println("Minimum number is "+min);
        return min;
    }

    int getMax(int[] array) {
        int max = array[0];
        for (int num : array) {
            if (num > max) {
                max = num;
            }

        }
        System.out.println("Maximum number is "+max);
        return max;
    }

}
