package HomeWorkThree.ObjInArray;

public class Soda {
    String brand;
    double volume;
    String color;
    int numOfIngridients;


    //Constructors
    Soda(String brandParam, double volumeParam, String colorParam, int numOfIngridientsParam) {
        brand = brandParam;
        volume = volumeParam;
        color = colorParam;
        numOfIngridients = numOfIngridientsParam;

    }

    Soda(String brandParam, double volumeParam) {
        brand = brandParam;
        volume = volumeParam;
    }

    Soda() {
        brand = "Regular Soda";
        volume = 0.5;
    }


    //Getters+Setters
    String getBrand() {
        return brand;
    }
    void setBrand(String brandParam){
        brand = brandParam;
    }
    double getVolume() {
        return volume;
    }
    void setVolume(double volumeParam){
        volume = volumeParam;
    }
    String getColor() {
        return color;
    }
    void setColor(String colorParam){
        color = colorParam;
    }
    int getNumOfIngridients(){
        return numOfIngridients;
    }
    void setNumOfIngridients(int numOfIngridientsParam){
        numOfIngridients=numOfIngridientsParam;
    }


}
