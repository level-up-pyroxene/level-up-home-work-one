package HomeWorkThree.ObjInArray;

public class Main {
    public static void main(String[] args) {

        Soda[]sodas;
        sodas = new Soda[2];
        sodas[0]= new Soda("CocaCola",1,"Red",30);
        sodas[1]= new Soda();
        sodas[1].setNumOfIngridients(10);

        //sout 1 soda with full param
        System.out.println(sodas[0].brand+" "+sodas[0].volume+" litre "+sodas[0].color+" "+sodas[0].numOfIngridients);
        //sout 2nd soda with default param
        System.out.println(sodas[1].brand+" "+sodas[1].volume+" liters "+sodas[1].color+" "+sodas[1].numOfIngridients);

    }
}
