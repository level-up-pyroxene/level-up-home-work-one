package bracers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Stack;

public class BracersFinder {


    public boolean isAllClosed(String bracerString) {

        char[] bracersArray = bracerString.trim().toCharArray();
        Stack<Character> bracerStack = new Stack<>();

        for (Character bracer : bracersArray) {
            if (bracer == '(' || bracer == '{' || bracer == '[' || bracer == '<') {
                bracerStack.push(bracer);
            }
            if (bracer == ')' || bracer == '}' || bracer == ']' || bracer == '>') {
                if (!bracerStack.empty()) {
                Character topCharInStack = bracerStack.peek();
                    switch (topCharInStack) {
                        case '(':
                            if (bracer == ')') {
                                bracerStack.pop();
                                break;
                            }
                        case '{':
                            if (bracer == '}') {
                                bracerStack.pop();
                                break;
                            }
                        case '[':
                            if (bracer == ']') {
                                bracerStack.pop();
                                break;
                            }
                        case '<':
                            if (bracer == '>') {
                                bracerStack.pop();
                                break;
                            }
                    }
                }else{
                    return !bracerStack.empty();
                }
            }
        }
        return bracerStack.empty();
    }
}
