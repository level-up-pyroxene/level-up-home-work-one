package builder;

public class Vape {

    private String brand;
    private String model;
    private String typeOfVape;
    private String color;
    private String material;
    private int fluidCapacity;
    private int numOfBatteries;
    private int numOfCoils;
    private int price;

//    private Vape(String brand, String model, String typeOfVape, String color, String material, int fluidCapacity, int numOfBatteries, int numOfCoils, int price) {
//        this.brand = brand;
//        this.model = model;
//        this.typeOfVape = typeOfVape;
//        this.color = color;
//        this.material = material;
//        this.fluidCapacity = fluidCapacity;
//        this.numOfBatteries = numOfBatteries;
//        this.numOfCoils = numOfCoils;
//        this.price = price;
//    }

    private Vape() { }

    public static VapeBuilder newBuilder(){
        return new Vape().new VapeBuilder();
    }

    @Override
    public String toString() {
        return "Vape{" +
                "brand='" + getBrand() + '\'' +
                ", model='" + getModel() + '\'' +
                ", typeOfVape='" + typeOfVape + '\'' +
                ", color='" + color + '\'' +
                ", material='" + material + '\'' +
                ", fluidCapacity=" + fluidCapacity +
                ", numOfBatteries=" + numOfBatteries +
                ", numOfCoils=" + numOfCoils +
                ", price=" + price +
                '}';
    }

    public class VapeBuilder {

        //VALIDATE() NEEDED IN BUILDER.BUILD TO CHECK IF VAPE IS VALID

        private Vape stub;

        public VapeBuilder() {
            stub = new Vape();
        }

        public VapeBuilder brand(String brand) {
            stub.setBrand(brand);
            return this;
        }

        public VapeBuilder model(String model) {
            stub.setModel(model);
            return this;
        }

        public VapeBuilder typeOfVape(String typeOfVape) {
            stub.setTypeOfVape(typeOfVape);
            return this;
        }

        public VapeBuilder color(String color) {
            stub.setColor(color);
            return this;
        }

        public VapeBuilder material(String material) {
            stub.setMaterial(material);
            return this;
        }

        public VapeBuilder fluidCapacity(int fluidCapacity) {
            stub.setFluidCapacity(fluidCapacity);
            return this;
        }

        public VapeBuilder numOfBatteries(int numOfBatteries) {
            stub.setNumOfBatteries(numOfBatteries);
            return this;
        }

        public VapeBuilder numOfCoils(int numOfCoils) {
            stub.setNumOfCoils(numOfCoils);
            return this;
        }

        public VapeBuilder price(int price) {
            stub.setPrice(price);
            return this;
        }

        public Vape build() {
            return stub;
//            return new Vape(stub.brand,stub.model,stub.typeOfVape,
//                            stub.color,stub.material,stub.fluidCapacity,
//                            stub.numOfBatteries,stub.numOfCoils,stub.price);
//            Vape finalVape = new Vape();
//            finalVape.brand = Vape.this.brand;
//            finalVape.color = Vape.this.color;
//            finalVape.fluidCapacity = Vape.this.fluidCapacity;
//            finalVape.material = Vape.this.material;
//            finalVape.model = Vape.this.model;
//            finalVape.numOfBatteries = Vape.this.numOfBatteries;
//            finalVape.numOfCoils = Vape.this.numOfCoils;
//            finalVape.typeOfVape = Vape.this.typeOfVape;
//            finalVape.price = Vape.this.price;
//            return finalVape;
        }
    }


    //Getters\Setters

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTypeOfVape() {
        return typeOfVape;
    }

    public void setTypeOfVape(String typeOfVape) {
        this.typeOfVape = typeOfVape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getFluidCapacity() {
        return fluidCapacity;
    }

    public void setFluidCapacity(int fluidCapacity) {
        this.fluidCapacity = fluidCapacity;
    }

    public int getNumOfBatteries() {
        return numOfBatteries;
    }

    public void setNumOfBatteries(int numOfBatteries) {
        this.numOfBatteries = numOfBatteries;
    }

    public int getNumOfCoils() {
        return numOfCoils;
    }

    public void setNumOfCoils(int numOfCoils) {
        this.numOfCoils = numOfCoils;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
