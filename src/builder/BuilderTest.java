package builder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.logging.FileHandler;

public class BuilderTest {
    public static void main(String[] args) {

        Vape.VapeBuilder vapeBuilder = Vape.newBuilder();
        Vape vapeBulded = vapeBuilder.brand("Wismec")
                .color("black")
                .fluidCapacity(10)
                .material("Metal")
                .model("RDA")
                .numOfBatteries(3)
                .numOfCoils(2)
                .price(3000)
                .build();

        System.out.println(vapeBulded.toString());


//        Class c = ArrayList.class;
//
//        Field[] fields = c.getDeclaredFields();
//
//        ArrayList<String> inspectedObject = new ArrayList<>();
//
//        for (Field f : fields) {
//            f.setAccessible(true);
//            f.getType();
//
//            f.get(inspectedObject);
//        }


    }
}
